﻿using HQ.eSkyFramework;
using HQFramework.DAL;
using Newtonsoft.Json;
using OM10100.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Web.Mvc;

namespace OM10100.Controllers
{
    public class CalcDiscountController : Controller
    {
        //
        // GET: /CalcDiscount/
        private static readonly log4net.ILog _logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private OM10100Entities _app = Util.CreateObjectContext<OM10100Entities>(false);
        private JsonResult _logMessage;
        private List<DiscountHeaderModel> nRetailCheckOutWithPromotion;
        private PromotionResponseModel responseModel = new PromotionResponseModel(true);
        public static void Update<T, F>(T Source, F Desc, bool isNew, string[] ignore = null)
        {
            var typeSource = Source.GetType();
            PropertyInfo[] propsSource = typeSource.GetProperties();

            var typeDesc = Desc.GetType();
            PropertyInfo[] propsDesc = typeDesc.GetProperties();
            var sys = new string[] { "Crtd_DateTime", "Crtd_Prog", "Crtd_User", "LUpd_DateTime", "LUpd_Prog", "LUpd_User", "tstamp" };

            foreach (var itemSource in propsSource)
            {
                var itemDesc = typeDesc.GetProperty(itemSource.Name);
                if (itemDesc != null && ignore != null && ignore.Contains(itemSource.Name)) continue;

                if (itemDesc != null && !sys.Contains(itemSource.Name))
                {
                    try
                    {
                        // _logger.Debug(itemSource.Name);
                        itemSource.SetValue(Source, itemDesc.GetValue(Desc, null), null);
                    }
                    catch
                    {
                        _logger.Error(itemSource.Name);
                    }

                }
            }

        }

        [AllowAnonymous]
        [HttpPost]
        public ActionResult CalcPromo4nRetail([System.Web.Http.FromBody] CalcnRetailPromotionModel calcnRetailPromotionModel)
        {
            try
            {
                var apiKey = Request.Headers["apiKey"].PassNull();
                if (apiKey != "oIXpIq3OQYW2dlpui7Kf_8msy41O9e2CejKOLfLTvVM")
                {
                    return new HttpUnauthorizedResult();
                }
                _logger.Debug(calcnRetailPromotionModel);
                string guiID = InsertOrderFrom_nRetail(calcnRetailPromotionModel.Order);
                Current.UserName = "ADMIN";
                Current.CpnyID = "nRetail";
                responseModel = new PromotionResponseModel(true);
                responseModel.VendorId = calcnRetailPromotionModel.Order.VendorId.ToString();
                responseModel.Message = string.Empty;
                responseModel.StatusCode = 0;
                var orderHeader = _app.OM10100_pdnRetailOrder(guiID).FirstOrDefault();
                var orderDets = _app.OM10100_pdnRetailOrderDet(guiID).ToList();
                if (calcnRetailPromotionModel.CalcnRetailPromotionType == CalcnRetailPromotionType.CheckPromotion)
                {
                    nRetailCheckOutWithPromotion = JsonConvert.DeserializeObject<List<DiscountHeaderModel>>(calcnRetailPromotionModel.DiscountHeaders);
                }
                OM10100.Controllers.OM10100Controller om10100 = new OM10100.Controllers.OM10100Controller();

                _logMessage = om10100.BeforeCalcPromo4nRetail(orderHeader, orderDets);

                if (_logMessage != null)
                {
                    return _logMessage;
                }
                return Util.CreateMessage(MessageProcess.Save, new { orderNbr = orderHeader.OrderNbr });
            }
            catch (Exception ex)
            {
                if (ex is MessageException)
                {
                    return (ex as MessageException).ToMessage();
                }
                if (ex.Message == "Choice" || ex.Message == "Budget")
                {
                    return _logMessage;
                }
                if (ex is OptimisticConcurrencyException)
                {
                    return (new MessageException(MessageType.Message, "5500056")).ToMessage();
                }
                return Util.CreateError(ex.ToString());
            }
        }

        public ActionResult Index()
        {
            return View();
        }
        private OM10100_pdInventoryByID_Result GetInventory(string invtID)
        {
            var objInvt = _app.OM10100_pdInventoryByID(invtID, "", Current.UserName, Current.CpnyID, Current.LangID).FirstOrDefault();
            if (objInvt == null)
            {
                objInvt = new OM10100_pdInventoryByID_Result();
            }
            return objInvt;
        }

        private string InsertOrderFrom_nRetail(Order order)
        {
            DataAccess dal = Util.Dal(false);
            #region -Table OM_PriceClassTmp-
            string[] orderColumns = new string[] { "Id", "UserId", "UserName", "City", "Street", "State", "Country", "ZipCode", "AddressFull",
                                                "ReceiverName", "PhoneNumber", "ShopId", "VendorId", "CreationDate", "OrderDate", "OrderNbr", "OrdAmt", "OrdQty"};
            System.Data.DataTable dtOrderTmp = new System.Data.DataTable() { TableName = "OM_RetailerOrder" };
            foreach (var col in orderColumns)
            {
                dtOrderTmp.Columns.Add(new DataColumn() { ColumnName = col });
            }
            // order.BasketItems.ToList().Sum(x => x.Units);
            string guid = Guid.NewGuid().ToString();
            var dtRowOrder = dtOrderTmp.NewRow();
            dtRowOrder["Id"] = guid;
            dtRowOrder["UserId"] = order.UserId.PassNull();
            dtRowOrder["UserName"] = order.UserName.PassNull();
            dtRowOrder["City"] = order.City.PassNull();
            dtRowOrder["Street"] = order.Street.PassNull();
            dtRowOrder["State"] = order.State.PassNull();
            dtRowOrder["Country"] = order.Country.PassNull();
            dtRowOrder["ZipCode"] = order.ZipCode.PassNull();
            dtRowOrder["AddressFull"] = order.AddressFull.PassNull();
            dtRowOrder["ReceiverName"] = order.ReceiverName.PassNull();
            dtRowOrder["PhoneNumber"] = order.PhoneNumber.PassNull();
            dtRowOrder["ShopId"] = order.ShopId;
            dtRowOrder["VendorId"] = order.VendorId;
            dtRowOrder["CreationDate"] = DateTime.Now;
            dtRowOrder["OrderDate"] = order.OrderDate;
            dtRowOrder["OrderNbr"] = order.OrderNbr;
            dtRowOrder["OrdAmt"] = order.GetTotalAmt();
            dtRowOrder["OrdQty"] = order.GetTotalQty();
            dtOrderTmp.Rows.Add(dtRowOrder);

            string[] orderDetailColumns = new string[] { "Id", "LineRef", "ProductId", "ProductName", "UnitPrice", "LineAmt", "Quantity", "Unit", "PictureUrl" };
            System.Data.DataTable dtOrderDetailsTmp = new System.Data.DataTable() { TableName = "OM_RetailerOrderDetail" };
            foreach (var col in orderDetailColumns)
            {
                dtOrderDetailsTmp.Columns.Add(new DataColumn() { ColumnName = col });
            }
            var basketItem = order.OrderItems.ToList();
            for (int i = 0; i < basketItem.Count; i++)
            {
                var det = basketItem[i];
                var dtRowDetails = dtOrderDetailsTmp.NewRow();
                dtRowDetails["Id"] = guid;
                dtRowDetails["LineRef"] = (i + 1);
                dtRowDetails["ProductId"] = det.ProductId;
                dtRowDetails["ProductName"] = det.ProductName.PassNull();
                dtRowDetails["UnitPrice"] = det.UnitPrice.ToDouble();
                dtRowDetails["LineAmt"] = det.GetLineAmt();
                dtRowDetails["Quantity"] = det.Units.ToDouble();
                dtRowDetails["Unit"] = det.Unit.PassNull();
                dtRowDetails["PictureUrl"] = det.PictureUrl;
                dtOrderDetailsTmp.Rows.Add(dtRowDetails);
            }
            // Trung bay + Tich luy + KM ..
            using (SqlConnection dbConnection = new SqlConnection(dal.ConnectionString))
            {
                dbConnection.Open();
                using (SqlTransaction sqlTran = dbConnection.BeginTransaction())
                {
                    using (SqlBulkCopy s = new SqlBulkCopy(dbConnection, SqlBulkCopyOptions.KeepIdentity, sqlTran))
                    {
                        try
                        {
                            // Insert vào bảng tạm Header
                            s.DestinationTableName = dtOrderTmp.TableName;
                            foreach (var col in dtOrderTmp.Columns)
                                s.ColumnMappings.Add(col.ToString(), col.ToString());
                            s.WriteToServer(dtOrderTmp);

                            using (SqlBulkCopy s2 = new SqlBulkCopy(dbConnection, SqlBulkCopyOptions.KeepIdentity, sqlTran))
                            {
                                try
                                {
                                    // Insert vào bảng tạm OM_FCSHeader
                                    s2.DestinationTableName = dtOrderDetailsTmp.TableName;
                                    foreach (var column in dtOrderDetailsTmp.Columns)
                                        s2.ColumnMappings.Add(column.ToString(), column.ToString());
                                    s2.WriteToServer(dtOrderDetailsTmp);
                                    sqlTran.Commit();
                                }
                                catch (Exception ex)
                                {
                                    _logger.Debug("@@Error insert table: " + ex.ToString());
                                    // Rollback khi có lỗi
                                    sqlTran.Rollback();
                                    throw ex;
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            _logger.Debug("@@Error insert table: " + ex.ToString());
                            sqlTran.Rollback();
                            throw ex;
                        }
                    }
                }
            }
            #endregion
            return guid;
        }
    }
}
