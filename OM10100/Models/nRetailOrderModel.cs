﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OM10100.Models
{
    public enum CalcnRetailPromotionType
    {
        CalcPromotion,
        CheckPromotion
    }
    public class CalcnRetailPromotionModel
    {
        public Order Order { get; set; }
        public string DiscountHeaders { get; set; }
        public CalcnRetailPromotionType CalcnRetailPromotionType { get; set; }
    }
    public class BasketItem
    {
        public int ProductId { get; set; }

        public string ProductName { get; set; }

        public decimal UnitPrice { get; set; }

        public decimal Discount { get; set; }

        public decimal Units { get; set; }
        public string Unit { get; set; }
        public string PictureUrl { get; set; }
        public decimal GetLineAmt()
        {
            return Units * UnitPrice;
        }
    }
    public class Order
    {
        public string UserId { get; set; }
        public string UserName { get; set; }
        public string City { get; set; }
        public string Street { get; set; }
        public string State { get; set; }
        public string Country { get; set; }
        public string ZipCode { get; set; }
        public IList<BasketItem> OrderItems { get; set; }
        public string AddressFull { get; set; }
        public string ReceiverName { get; set; }
        public string PhoneNumber { get; set; }
        public int ShopId { get; set; }
        public int VendorId { get; set; }
        public DateTime OrderDate { get; set; }
        public string Id { get; set; }
        public DateTime CreationDate { get; set; }
        public int OrderNbr { get; set; }
        public double OrderAmt { get; set; }
        public double OrderQty { get; set; }
        public decimal GetTotalAmt()
        {
            return OrderItems.Sum(o => o.Units * o.UnitPrice);
        }
        public decimal GetTotalQty()
        {
            return OrderItems.Sum(o => o.Units);
        }
    }    
}