﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OM10100.Models
{
    public class OrderPromotion
    {
    }
    public class SitePromotionNotEnough
    {
        public string InvtID { get; set; }
        public string SiteID { get; set; }
        public string UOM { get; set; }
        public string DiscID { get; set; }
        public string DiscSeq { get; set; }
        public double FreeQty { get; set; }
        public double FreeQtyPromotion { get; set; }
    }
}