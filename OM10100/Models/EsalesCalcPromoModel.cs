﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OM10100.Models
{
    public class EsalesCalcPromoModel
    {
        public string BranchID { get; set; }
        public string SlsperID { get; set; }
        public string CustID { get; set; }
        public CalcnRetailPromotionType CalcnRetailPromotionType { get; set; }
        public string DiscountHeaders { get; set; }
        public string OrderType { get; set; }
    }
}