﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OM10100.Models
{
    public class PromotionResponseModel
    {
        public string VendorId { get; set; }
        public int StatusCode { get; set; }
        public string Message { get; set; }
        public string ErrorProduct { get; set; }
        public List<DiscountHeaderModel> DiscountHeaders { get; set; }
        public PromotionResponseModel(bool isDefault)
        {
            this.DiscountHeaders = new List<DiscountHeaderModel>();
        }
        public PromotionResponseModel()
        {
            this.DiscountHeaders = new List<DiscountHeaderModel>();
        }
    }
    public class DiscountHeaderModel
    {                
        public string DiscID { get; set; }
        public string DiscSeq { get; set; }
        public string DiscSeqDescr { get; set; }                    
        public string BreakBy { get; set; } // Số Tiền/ Số Lượng
        public string DiscFor { get; set; }  // Khuyến Mãi theo     
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public bool Promo { get; set; } 
        public double QtyAmt { get; set; } // Số Lượng/ Số Tiền Mua thỏa chương trình này
        public double DisctblAmt { get; set; }
        public double DisctblQty { get; set; }
        public double QtyAmtUsed { get; set; } // Số Lượng/ Số Tiền được KM
        public string GroupLineRef { get; set; }   // Những dòng sản phẩm mua thỏa chương trình
        public string ProductApply { get; set; }
        public List<DiscBreak> DiscBreaks { get; set; }
        public bool AutoFreeItem { get; set; }
        public string DiscType { get; set; }
        public string BudgetId { get; set; }
        public bool DonateGroupProduct { get; set; }
        public DiscountHeaderModel()
        {
        }
        public DiscountHeaderModel(string discID, string discSeq, string discType, string discSeqDescr, double disctblAmt
                    , double disctblQty, string breakBy, string discFor, 
                    DateTime startDate, DateTime? endDate, bool promo, double qtyAmt, string groupLineRef, string productApply
                    , bool autoFreeItem                    
                    , List<DiscBreak> discBreaks, string budgetId, bool donateGroupProduct)
        {
            DiscID = discID;
            DiscSeq = discSeq;
            DiscType = discType;
            DiscSeqDescr = discSeqDescr;
            BreakBy = breakBy;
            DiscFor = discFor;
            StartDate = startDate;
            EndDate = endDate;
            Promo = promo;
            QtyAmt = qtyAmt;
            GroupLineRef = groupLineRef;
            ProductApply = productApply;
            DiscBreaks = discBreaks;
            AutoFreeItem = autoFreeItem;
            DisctblAmt = disctblAmt;
            DisctblQty = disctblQty;
            BudgetId = budgetId;
            DonateGroupProduct = donateGroupProduct;
        }
    }

    public class DiscBreak
    {
        public bool IsChoiced { get; set; }
        public string LineRef { get; set; } // Mức khuyến mãi
        public string Descr { get; set; }
        public double DiscAmt { get; set; }
        public double BreakQtyAmt { get; set; }
        public double MaxLot { get; set; }
        public double Amount { get; set; } // Số tiền được KM      
        public List<DiscountFreeItem> DiscountFreeItems { get; set; }
        public string BudgetId { get; set; }
        public DiscBreak(bool isChoiced, string lineRef, string descr, double discAmt, double breakQtyAmt, double maxLot, double amount, List<DiscountFreeItem> discountFreeItems, string budgetId)
        {
            IsChoiced = isChoiced;
            LineRef = lineRef;
            Descr = descr;
            DiscAmt = discAmt;
            BreakQtyAmt = breakQtyAmt;
            MaxLot = maxLot;
            Amount = amount;
            DiscountFreeItems = discountFreeItems;
            BudgetId = budgetId;
        }
    }

    public class DiscountFreeItem
    {
        public bool IsChoiced { get; set; }
        public int ProductId { get; set; }
        // FreeItem Info            
        public string FreeItemID { get; set; }        
        public double FreeItemQty { get; set; }
        public string FreeItemDescr { get; set; }
        public string FreeItemUnit { get; set; }
        public double FreeItemQtyUsed { get; set; }
        public string BudgetId { get; set; }
        public DiscountFreeItem(bool isChoiced, int productId, string freeItemID, double freeItemQty, string freeItemDescr, string freeItemUnit, double freeItemQtyUsed, string budgetId)
        {
            IsChoiced = isChoiced;
            ProductId = productId;
            FreeItemID = freeItemID;
            FreeItemQty = freeItemQty;
            FreeItemDescr = freeItemDescr;
            FreeItemUnit = freeItemUnit;
            FreeItemQtyUsed = freeItemQtyUsed;
            BudgetId = budgetId;
        }
    }
}