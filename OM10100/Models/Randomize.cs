﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OM10100.Models
{
    public class Item
    {
        public int FortuneID; // not only string, any type of data
        public string DiscID;
        public string DiscSeq;
        public int Probability;  // chance of getting this Item
    }
    public class ProportionalWheelSelection
    {
        public static Random rnd = new Random(DateTime.Now.Millisecond);

        // Static method for using from anywhere. You can make its overload for accepting not only List, but arrays also: 
        // public static Item SelectItem (Item[] items)...
        public static Item SelectItem(List<Item> items)
        {
            // Calculate the summa of all portions.
            int poolSize = 0;
            for (int i = 0; i < items.Count; i++)
            {
                poolSize += items[i].Probability;
            }

            // Get a random integer from 0 to PoolSize.
            int randomNumber = rnd.Next(0, poolSize) + 1;

            // Detect the item, which corresponds to current random number.
            int accumulatedProbability = 0;
            for (int i = 0; i < items.Count; i++)
            {
                accumulatedProbability += items[i].Probability;
                if (randomNumber <= accumulatedProbability)
                    return items[i];
            }
            return null;    // this code will never come while you use this programm right :)
        }
    }
}